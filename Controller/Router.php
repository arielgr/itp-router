<?php
namespace Gamma\Routing\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

class Router implements RouterInterface
{

    protected $actionFactory;


    public function __construct(
        ActionFactory $actionFactory
    )
    {
        $this->actionFactory = $actionFactory;
    }


    public function match(RequestInterface $request)
{
    $identifier = trim($request->getPathInfo(), '/');
        if($identifier === 'route-testing') {
            return $this->actionFactory->create
            ('Gamma\Routing\Controller\Testing\Index');
    }else return null;

}


}

