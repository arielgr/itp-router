<?php

namespace Gamma\Routing\Controller;

use Magento\Cms\Model\PageRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;


class HomeRouter implements RouterInterface
{

    protected $actionFactory;
    protected $pageRepository;
    protected $searchCriteriaBuilder;
    protected $list;

    public function __construct(
        ActionFactory $actionFactory,
        PageRepository $pageRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->actionFactory = $actionFactory;
        $this->pageRepository = $pageRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }


    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        $pages = $this->pageRepository->getList($this->searchCriteriaBuilder->create())->getItems();
        $data = [];

        foreach ($pages as $page){
            //Print the static pages
            //var_dump($page->getIdentifier());
            $data[] = $page->getIdentifier();
        }

        if(in_array($identifier, $data)) {
            return $this->actionFactory->create
            ('Magento\Cms\Controller\Index\Index');
        }else return null;

    }
}