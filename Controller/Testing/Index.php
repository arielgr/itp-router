<?php

namespace Gamma\Routing\Controller\Testing;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

Class Index extends Action {

    protected $output;
    public function __construct(Context $context)
    {
        return parent::__construct($context);

    }

    public function execute()
    {
        echo "HELLO, You are executing Controller/Testing/Index";
    }
}